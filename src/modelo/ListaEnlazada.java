package modelo;

//import Lista_Simple.*;

/**
 *
 * @author
 */
public class ListaEnlazada {

    private Nodo Primero; //La lista solo tiene una cabecera por que es de alli 
                  //donde estan enlazadas todos y cada uno de los Nodos.
    
    /**
     * Metodo que crea una Lista
     */
    public ListaEnlazada() {
        Primero = null;
    }
    
    //Metodos get y set... =====================================================
    /**
     * Metodo que me sirve para establecer un primero en la cabecera de 
     * la lista, utilizo este metodo por que es necesario gracias al 
     * encapsulammiento de datos en un objeto.
     * @param Primero 
     */
    public void setPrimero(Nodo Primero) {
        this.Primero = Primero;
    }
    
    /**
     * Metodo que me sirve para devolver la cabecera de esta lista.
     * @return 
     */
    public Nodo getPrimero() {
        return Primero;
    }
    //==========================================================================
    
    /**
     * Metodo que me permite saber si la lista esta vacia o no.
     * @return <code> boolean </code> value.
     */
    public boolean vacia() {
        if (Primero == null) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Metodo que crea un nodo temporal, sin asignar a ninguna parte, luego ese 
     * nodo temporal se le asigna el primero de la lista y despues, ese nodo 
     * temporal con su nodo enlazado ahora sera el nuevo nodo de la lista Total
     * llamado nodo "primero".
     * @param Dato 
     */
    public void insertarEnPrimerLugar(Nodo nd){
        Nodo Temporal = nd; //crea el nodo Temporal
        Temporal.setSiguiente(Primero); //Nos ubica en el primer nodo de la Lista.
        Primero = Temporal; //Ahora coloca el nodo Temporal que se creo en 
        //este metodo , como el nuevo, nodo de la lista, es decir lo movió. 
    }
    
    /**
     * Este metodo me permite borrar el primer nodo, enlazandolo al siguiente
     */
    public void borrarPrimero(){
        Primero = Primero.getSiguiente();
    }
    
    /**
     * Metodo que me permite mostrar todos los elementos 
     * de la lista que estoy creando.
     * @return <code> La lista </code> String.
     */
    public String mostrarLista(){
        String Dato = "";
        Nodo aux = Primero;
        
        while (aux != null) {            
            Dato += "[" +aux.getInfo()+ "]";
            aux = aux.getSiguiente();
        }
        
        return Dato;
    } 
}
