
package modelo;

//Todos los Nodos tienen 2 componentes, su Dato y su enlace.

//import Lista_Simple.*;
//Todos los Nodos tienen 2 componentes, su Dato y su enlace.

/**
 * Esta clase me permite ejemplificar de manera 
 * sencilla un nodo informatico en una lista.
 * @author Leonardo Collazos
 */
public class Nodo {
    
    //atributos
    private int info; //para este ejeplo en BD modifique un poco esta estructura 
                      //y lo que hice fue darle un tipo privado a la informacion 
                      //del nodo para poder accesar por medio de un getInfo.
    private Nodo siguiente; //Este es el enlace al siguiente nodo; (este tambien lo cambie poniendolo private)
    
    
    /**
     * metodo para crear un nodo sin pasarle parametro.
     * @param dato 
     */
    public Nodo() {
        info = 0; 
    }
    
    
    /**
     * metodo para crear un nodo, pasandole el parametro 
     * de informacion, que en este caso se llama dato.
     * @param dato 
     */
    public Nodo(int dato) {
        info = dato; 
    }
    
    
    //aqui estan los metodos get y set que le aumente a esta clase ===========
    
    public void setInfo(int info) {
        this.info = info;
    }
    
    public void setSiguiente(Nodo siguiente) {
        this.siguiente = siguiente;
    }

    public int getInfo() {
        return info;
    }

    public Nodo getSiguiente() {
        return siguiente;
    }
    
    //========================================================================
    
    /**
     * Metodo para ver la informacion de un nodo.
     */
    public void verNodo(){
        System.out.print("{ "+info+" }");
    }
    
}
