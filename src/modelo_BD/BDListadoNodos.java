package modelo_BD;

import Control_BD.ConexionConBaseDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.ListaEnlazada;
import modelo.Nodo;
import vista.agregarDato;

/**
 *
 * @author
 */
public class BDListadoNodos {

    public static void guardarRegistro(Nodo nd) {
        Connection reg = ConexionConBaseDatos.getConexion();

        String sql = "INSERT INTO listaenlazada(dato_lista) VALUES (?);";
        try {

            PreparedStatement pst = reg.prepareStatement(sql);

            pst.setInt(1, nd.getInfo());

            int n = pst.executeUpdate();
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Registrado Exitoso de Estudiante");
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error - " + ex);
            Logger.getLogger(agregarDato.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//cierra metodo agregarEstudiante

    public static void guardarVariosRegistros(ListaEnlazada lstNodo) {
        //Estudiante est = lstCurso.getPrimero();
        //Nodo nd = ListaEnlazada.
        Nodo nd = lstNodo.getPrimero();
        while (nd != null) {
            guardarRegistro(nd);
            nd = nd.getSiguiente();
        }

    }
}
