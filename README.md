# README #

Para recrear el funcionamiento de este proyecto se debe de descargar el repositorio y tener java + el JVM funcionando en el equipo
adicionalmente se debe de tener instalado MariaDB (anteriormente llamado MySQL) para que las transacciones a la base de datos 
funcionen de igual manera.

### ¿Para qué es este repositorio? ###

Este repositorio es para repaso de las Listas en Java + un pequeño GUI que sirve para visualizar facilmente las acciones basicas.
Version 0.0.1

### Como lo configuro para que funcione? ###

Primero tener en cuenta que se debe tener el JVM + JDK-7 u 8
Su Entorno IDE Favorito en mi caso utilice Netbeans 8.0.2
Ninguna Dependencia
Configuracion de Base de datos Maria DB
No hay Tests actualmente

### Con quien comunicarse? ###
comunicarse con el administrador de este perfil para mas info
