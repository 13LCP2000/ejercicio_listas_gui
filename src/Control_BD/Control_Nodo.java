package Control_BD;

//import com.mysql.jdbc.EscapeTokenizer;
import jdk.nashorn.internal.ir.Node;
import modelo.ListaEnlazada;
import modelo.Nodo;
import modelo_BD.BDListadoNodos;
import vista.agregarDato;

/**
 *
 * @author
 */
public class Control_Nodo {

    private     Nodo      objNodo;
    private ListaEnlazada objListadoNodos;
    
    // ============================================================

    public Control_Nodo() {
            objNodo     = new Nodo();
        objListadoNodos = new ListaEnlazada();
    }
     
    // ================= Metodos Set y Get ========================
    public void setObjNodo(Nodo objNodo) {
        this.objNodo = objNodo;
    }

    public void setObjListadoNodos(ListaEnlazada objListadoNodos) {
        this.objListadoNodos = objListadoNodos;
    }

    public Nodo getObjNodo() {
        return objNodo;
    }

    public ListaEnlazada getObjListadoNodos() {
        return objListadoNodos;
    }
    // ============================================================

    public void agregarNodo_ListaTemp(int info) {
        
        objNodo = new Nodo();
        objNodo.setInfo(info);
        
        objListadoNodos.insertarEnPrimerLugar(objNodo);
    }

    public void agregarNodo_BD(Nodo nd) {
        //mando un objeto nodo de una lista temporal...
        BDListadoNodos.guardarRegistro(nd); // a guardarse en la Base de datos.
        //hago esto con uno por uno de los datos.
    }

    public void agregarVariosNodos_BD(ListaEnlazada lstNlzd) {
        //Estudiante est=lstCurso.getPrimero();
        Nodo nd = lstNlzd.getPrimero();
        while (nd != null) {
            agregarNodo_BD(nd);
            nd = nd.getSiguiente();
        }
    }
}
